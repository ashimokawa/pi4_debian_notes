# Purpose

Setup a headless Raspberry Pi 4 without microSD card, to use as a home server for different purposes using pure Debian (no Raspberry Pi OS)

NOTE: These instructions are mainly for myself, but might be helpful for others. If you notice mistakes please file an issue or open a PR.

## Hardware used

- Raspberry Pi 4 8GB
- Official Power Adapter
- USB3 UAS capable SATA Adapter
- 500GB Samsung EVO 850

## Initial installation

### Getting the "image"

It is possible to install the system using the Debian installer for aarch64 when using a chain-loaded UEFI firmware with ACPI tables, but since this is not what Debian uses as a default for the Pi4 and it seems a bit more complicated I chose to get a minimal image to get started here:

```
$ wget https://raspi.debian.net/daily/raspi_4_bullseye.img.xz

```

### Writing the image to the SSD

Connect the SSD via USB adapter to a Linux machine and do:

```
$ unxz raspi_4_bullseye.img.xz
$ dd if=raspi_4_bullseye.img of=/dev/sdX bs=4M 
``` 

Now you could just plug in the SSD via USB and boot, if you have a recent boot firmware on your Pi4, this was already the case for mine. If not you might have to update it via raspbian and microSD card.

The Debian image is *very* minimal, it has a root user without password, which won't work via ssh, if you don't want to plug in a monitor and keyboard like me do the following before your first boot while the SSD is still plugged into the Linux machine.

```
$ mkpasswd # enter a password and copy the resulting hash to your clipboard
$ mount /dev/sdX2 /mnt
$ edit /mnt/etc/shadow # paste the hash after root: 
$ edit /mnn/etc/ssh/sshd_config # add "PermitRootLogin yes" to the file 
```

Now if you first boot the partition will be expanded to full size, we will resize it later again, which only takes a few seconds (assuming you don't fill it up before doing so).

After the operation is complete you should be able to login to your Pi4 via ssh.

You can now add a user install stuff, install sudo, disable the root user etc, your microSD-less Debian 64bit OS is ready and you are done. Read on if you want to encrypt your home partition.

## Disk Encryption

For my purpose I decided against full disk encryption, since I want to have some services on the Pi4 which should not require entering a password at boot, also since it is headless I would have to use ssh to connect to the initrd, so I only wanted to encrypt one partition of the SSD, services would then have to be started only after the necessary password has been entered. But still essential things (like a DNS server) would still run after automatically a power failure.

### Resize the partiion and create a new one

Connect the SSD again via USB to your Linux machine and use "gparted" to resize the partition again. Make sure no partition is mounted (if it auto-mounts unmount it). Start gparted chose the drive and resize the second partition (named RASPI_BOOT when you used the image linked above) to 100GB (more or less according to your taste). Make the rest a new partition. The operation will only take a few seconds. 

### Create the crypto partition

The Pi4 does not have hardware AES acceleration, so it is wise to choose a cipher that wont be to heavy on the CPU. 

With the USB3/SATA adapter and SSD mentioned, write speed performance degradation is acceptable when measuring with nench.sh:

- Without encryption: 167.53 MiB/s
- With encryption: 143.05 MiB/s

Boot your Pi4 again from the SSD and as root issue the following commands:


```
$ apt install cryptsetup
$ cryptsetup --type luks2 --cipher xchacha20,aes-adiantum-plain64 --hash sha256 --iter-time 5000 --key-size 256 --pbkdf argon2i luksFormat /dev/sda3
```

Be sure to remember the password you enter - if you loose it, all your data is lost.

Now it is time to format the disk and choose a mountpoint for the device. I chose /crypt.

```
$ cryptsetup luksOpen /dev/sda3 crypt # this will ask for the password
$ mkfs.ext4 /dev/mapper/crypt
$ mkdir /crypt
$ mount /dev/mapper/crypt /crypt
```

Now after every boot we need to execute cryptsetup and mount after every boot. I made a small shell script, which does it and which I plan also to use for launching services that depend on /crypt being mounted

I edited /usr/local/bin/startup.sh at put the following inside (for now)

```
#!/bin/sh
cryptsetup --allow-discards luksOpen /dev/sda3 crypt && mount /dev/mapper/crypt /crypt
```

## Misc fixes

### Fix periodic errors in kernel log when no microSD card is inserted

This will break microSD card support entirely but if you do not want to use it anyway do this:

```
$ echo blacklist sdhci_iproc > /etc/modprobe.d/blacklist.conf 
$ update-initramfs -k all -u
```

### Make bluetooth work

```
$ apt install wget
$ wget https://github.com/armbian/firmware/raw/master/BCM4345C0.hcd -O /lib/firmware/brcm/BCM4345C0.hcd
$ reboot
```
